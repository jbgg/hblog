publicdir = .public

htmls = $(publicdir)/index.html

all : $(publicdir) $(htmls)

$(publicdir) :
	mkdir -p $(publicdir)

$(publicdir)/%.html : %.adoc
	asciidoctor $< -D $(publicdir)

clean :
	rm -rf $(publicdir)
